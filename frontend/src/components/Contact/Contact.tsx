import type { Component } from "solid-js";
import { createSignal, Switch, Match, Show } from "solid-js";

import styles from "./Contact.module.css";

import { put, del, post } from "../../utils/api";

const Contact: Component = (props: any) => {
  const [edit, setEdit] = createSignal(false);
  const [name, setName] = createSignal(props.name || "");
  const [address, setAddress] = createSignal(props.address || "");
  const [email, setEmail] = createSignal(props.email || "");

  const handleChange = (type: string) => (e) => {
    if (type === "name") setName(e.target.value);
    if (type === "address") setAddress(e.target.value);
    if (type === "email") setEmail(e.target.value);
  };

  const handleSave = () => {
    setEdit(!edit());
    if (props.create) {
      put({
        id: 0,
        name: name(),
        address: address(),
        email: email(),
      });
      setName("");
      setAddress("");
      setEmail("");
    } else {
      post({
        id: props.id,
        name: name(),
        address: address(),
        email: email(),
      });
    }
    props.refetch();
  };

  const handleDelete = () => {
    del({
      id: props.id,
      name: name(),
      address: address(),
      email: email(),
    });
    props.refetch();
  };

  return (
    <div class={styles.Contact}>
      <Switch fallback={<div>Loading...</div>}>
        <Match when={!edit() && !props.create}>
          <p>Name {name()}</p>
          <p>Address {address()}</p>
          <p>Email {email()}</p>
          <button type="button" onClick={() => setEdit(!edit())}>
            Edit
          </button>
        </Match>
        <Match when={edit() || props.create}>
          <input
            type="text"
            value={name()}
            onChange={handleChange("name")}
            placeholder="Name"
          />
          <input
            type="text"
            value={address()}
            onChange={handleChange("address")}
            placeholder="Address"
          />
          <input
            type="text"
            value={email()}
            onChange={handleChange("email")}
            placeholder="Email"
          />
          <button type="button" onClick={handleSave}>
            Save
          </button>
        </Match>
      </Switch>
      <Show when={!props.create}>
        <button type="button" onClick={handleDelete}>
          Delete
        </button>
      </Show>
    </div>
  );
};

export default Contact;
