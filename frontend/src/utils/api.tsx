import { url } from "./constants";

interface Contact {
  id: number;
  name: string;
  address: string;
  email: string;
}

export const fetchData = async () => {
  const res = await fetch(url);
  return await res.json();
};

export const put = async (contact: Contact) => {
  const res = await fetch(url, {
    method: "PUT",
    body: JSON.stringify(contact),
  });
  return await res.json();
};

export const del = async (contact: Contact) => {
  const res = await fetch(url, {
    method: "DELETE",
    body: JSON.stringify(contact),
  });
  return await res.json();
};

export const post = async (contact: Contact) => {
  const res = await fetch(url, {
    method: "POST",
    body: JSON.stringify(contact),
  });
  return await res.json();
};
