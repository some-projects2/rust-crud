import type { Component } from "solid-js";
import { createResource, For } from "solid-js";

import Contact from "./components/Contact/Contact";
import { fetchData, put } from "./utils/api";

import styles from "./App.module.css";

const App: Component = () => {
  const [data, { mutate, refetch }] = createResource(fetchData);

  return (
    <div class={styles.App}>
      <h1>Address Book</h1>
      <div class={styles.AddressBook}>
        <For each={data()} fallback={"Loading..."}>
          {(item) => (
            <Contact
              id={item.id}
              name={item.name}
              address={item.address}
              email={item.email}
              refetch={refetch}
            />
          )}
        </For>
        <Contact create={true} refetch={refetch} />
      </div>
    </div>
  );
};

export default App;
