pub mod api {
  use serde::{Deserialize, Serialize};

  use std::error::Error;
  use std::fs;
  use std::fs::File;
  use std::io::BufReader;
  use std::path::Path;

  use hyper::body::Bytes;
  use futures::{Future, Stream};
  use std::sync::atomic::{AtomicUsize, Ordering};
  // use std::Clone;

  #[derive(Serialize, Deserialize, Debug, Clone)]
  struct Contact {
    id: usize,
    name: String,
    address: String,
    email: String,
  }
  
  pub fn get() -> String {
    println!("GET /");
    
    let address_book: Vec<Contact> = read_json();

    for c in address_book.iter() {
      println!("Name is {}", c.name);
    }
    serde_json::to_string(&address_book).unwrap()
  }

  // pub fn put(contact: hyper::body::Bytes) -> String {
  pub fn put(contact: hyper::body::Bytes) -> String {
    println!("PUT /");
    let json_file_path = Path::new("./address_book.json");

    let mut c: Contact = serde_json::from_slice(&contact).unwrap();

    println!("c is {:?}", c);

    // read address_book    
    let mut address_book: Vec<Contact> = read_json();
    c.id = get_id();
    address_book.push(c);
    fs::write(json_file_path, serde_json::to_string(&address_book).unwrap());

    "PUT /".to_string()
  }

  pub fn delete(contact: hyper::body::Bytes) -> String {
    let json_file_path = Path::new("./address_book.json");
    
    let c: Contact = serde_json::from_slice(&contact).unwrap();
    let mut address_book: Vec<Contact> = read_json();
    address_book.retain(|x| x.id != c.id);
    fs::write(json_file_path, serde_json::to_string(&address_book).unwrap());

    println!("Id to delete is {}", c.id);
    "DELETE /".to_string()
  }

  pub fn post(contact: hyper::body::Bytes) -> String {
    let json_file_path = Path::new("./address_book.json");
    let c: Contact = serde_json::from_slice(&contact).unwrap();
    let id = c.id;
    let mut address_book: Vec<Contact> = read_json();
    for elem in address_book.iter_mut() {
      if elem.id.eq(&id) {
        *elem = c.clone();
      }
    }
    fs::write(json_file_path, serde_json::to_string(&address_book).unwrap());
    "POST /".to_string()
  }

  fn read_json() -> Vec<Contact> {
    let json_file_path = Path::new("./address_book.json");
    let file = File::open(json_file_path).unwrap();
    let reader = BufReader::new(file);
    
    let address_book: Vec<Contact> = serde_json::from_reader(reader).unwrap();
    address_book
  }

  fn get_id() -> usize {
    static COUNTER:AtomicUsize = AtomicUsize::new(1);
    COUNTER.fetch_add(1, Ordering::Relaxed)
  }
}