use std::{convert::Infallible, net::SocketAddr};
use hyper::{Body, Request, Response, Server};
use hyper::service::{make_service_fn, service_fn};
use hyper::{Method, StatusCode};
use hyper::body;

use backend::api;

async fn handle(req: Request<Body>) -> Result<Response<Body>, Infallible> {
    let mut response = Response::new(Body::empty());

    
    match (req.method(), req.uri().path()) {
        
        (&Method::GET, "/") => {
            *response.body_mut() = Body::from(api::get());
        },
        (&Method::PUT, "/") => {
            // we'll be back
            *response.body_mut() = Body::from(api::put(body::to_bytes(req.into_body()).await.unwrap()))
        },
        (&Method::DELETE, "/") => {
            *response.body_mut() = Body::from(api::delete(body::to_bytes(req.into_body()).await.unwrap()))
        },
        (&Method::POST, "/") => {
            *response.body_mut() = Body::from(api::post(body::to_bytes(req.into_body()).await.unwrap()))
        },
        _ => {
            *response.status_mut() = StatusCode::NOT_FOUND;
        },
    };

    Ok(response)
}

#[tokio::main]
async fn main() {
    let addr = SocketAddr::from(([127, 0, 0, 1], 3001));

    let make_svc = make_service_fn(|_conn| async {
        Ok::<_, Infallible>(service_fn(handle))
    });

    let server = Server::bind(&addr).serve(make_svc);

    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}